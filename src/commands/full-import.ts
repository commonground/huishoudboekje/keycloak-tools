import ClientRepresentation from "@keycloak/keycloak-admin-client/lib/defs/clientRepresentation";
import RealmRepresentation from "@keycloak/keycloak-admin-client/lib/defs/realmRepresentation";
import UserRepresentation from "@keycloak/keycloak-admin-client/lib/defs/userRepresentation";
import {CommandModule} from "yargs";
import getClient from "../../client";
import clients from "../../data/clients.json";
import users from "../../data/users.json";
import getEnv from "../getenv";

const fullImport: CommandModule = {
	command: "full-import",
	describe: "Perform a full import of all the data into Keycloak.",
	handler: async () => {
		const env = getEnv();
		const {KEYCLOAK_REALM_NAME} = env;

		if (!KEYCLOAK_REALM_NAME) {
			throw new Error("Missing environment variable KEYCLOAK_REALM_NAME.");
		}

		const kc = await getClient();

		const things = {
			getImportedUsers: (): UserRepresentation[] => {
				return users || [];
			},
			getImportedClients: (): ClientRepresentation[] => {
				return clients || [];
			},
			createOrUpdateUser: async (newUser: UserRepresentation) => {
				const findUser = await kc.users.find({
					email: newUser.email,
				});

				const foundUser = findUser?.[0];

				// Create user if it doesn't exist
				if (!foundUser) {
					try {
						const user = await kc.users.create(newUser);
						console.log(`Created user ${newUser.email} (${user.id})`);
					}
					catch (err) {
						console.log(err);
					}
				}
				else {
					console.log(`User ${findUser[0].email} already exists`);

					// Update user with new data
					// try {
					// 	await kc.users.update({
					// 		id: foundUser.id!,
					// 	}, newUser);
					// 	console.log(`Updated user ${newUser.email} (${foundUser.id})`);
					// }
					// catch (err) {
					// 	console.log(err);
					// }
				}
			},
			createClientIfNotExists: async (newClient: ClientRepresentation) => {
				// Check if client exists
				const findClient: ClientRepresentation[] | undefined = await kc.clients.find({
					clientId: newClient.clientId,
				});

				const foundClient = findClient?.[0];

				// Create client if it doesn't exist
				if (!foundClient) {
					const client = await kc.clients.create(newClient);
					console.log(`Created client ${newClient.clientId} (${client.id}).`);
				}
				else {
					console.log(`Client ${newClient.clientId} already exists`);

					// Update client with new data
					// try {
					// 	await kc.clients.update({
					// 		id: foundClient.id!,
					// 	}, newClient);
					// 	console.log(`Updated client ${newClient.clientId} (${foundClient.id})`);
					// }
					// catch (err) {
					// 	console.log(err);
					// }
				}
			},
		};

		// Check if the realm exists
		const findRealm: RealmRepresentation | undefined = await kc.realms.findOne({realm: KEYCLOAK_REALM_NAME});
		if (!findRealm) {
			const createdRealm = await kc.realms.create({realm: KEYCLOAK_REALM_NAME, enabled: true});
			console.log(`Created realm ${createdRealm.realmName}.`);
		}

		const realm: RealmRepresentation | undefined = await kc.realms.findOne({realm: KEYCLOAK_REALM_NAME});
		if (!realm) {
			throw new Error("Realm not found");
		}

		console.log(`Using realm ${realm.realm}.`);

		// Set the realm as default
		kc.setConfig({
			realmName: realm.realm,
		});

		const newUsers: UserRepresentation[] = things.getImportedUsers();

		console.log(`The following users will be created in the ${realm.realm} realm:`);
		console.table(newUsers);

		// Create users
		const createdNewUsers = newUsers.map(newUser => things.createOrUpdateUser(newUser));
		await Promise.all(createdNewUsers);

		console.log("Users created.");

		const newClients: ClientRepresentation[] = things.getImportedClients();

		console.log(`The following clients will be created in the ${realm.realm} realm:`);
		console.table(newClients);

		const createdClients = newClients.map(newClient => things.createClientIfNotExists(newClient));
		await Promise.all(createdClients);
	},
};

export default fullImport;
