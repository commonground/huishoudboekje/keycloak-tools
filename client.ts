import KcAdminClient from "@keycloak/keycloak-admin-client";
import getEnv from "./src/getenv";

// Check if all the required environment variables are set
const env = getEnv();
let {
	KEYCLOAK_REALM_NAME,
	KEYCLOAK_BASE_URL,
	KEYCLOAK_MASTER_REALM_NAME,
	KEYCLOAK_ADMIN_USERNAME,
	KEYCLOAK_ADMIN_PASSWORD,
} = env;

if (!KEYCLOAK_REALM_NAME) {
	throw new Error("Environment variable KEYCLOAK_REALM_NAME is missing.");
}
if (!KEYCLOAK_BASE_URL) {
	throw new Error("Environment variable KEYCLOAK_BASE_URL is missing.");
}
if (!KEYCLOAK_MASTER_REALM_NAME) {
	console.warn("Environment variable KEYCLOAK_MASTER_REALM_NAME is missing. Assuming 'master' as default.");
	KEYCLOAK_MASTER_REALM_NAME = "master";
}
if (!KEYCLOAK_ADMIN_USERNAME) {
	console.warn("Environment variable KEYCLOAK_ADMIN_USERNAME is missing. Assuming 'admin' as default.");
	KEYCLOAK_ADMIN_USERNAME = "admin";
}
if (!KEYCLOAK_ADMIN_PASSWORD) {
	console.warn("Environment variable KEYCLOAK_ADMIN_PASSWORD is missing. Assuming 'admin' as default value.");
	KEYCLOAK_ADMIN_PASSWORD = "admin";
}

const getClient = async () => {
	// Initialize the Keycloak Admin Client
	const kc = new KcAdminClient({
		baseUrl: KEYCLOAK_BASE_URL,
		realmName: KEYCLOAK_MASTER_REALM_NAME,
	});

	// Wait for Keycloak to be online
	while (true) {
		console.log("Waiting for Keycloak to startup...", KEYCLOAK_BASE_URL);

		const isKeycloakOnline = await new Promise(async resolve => {
			try {
				// Authorize as admin
				await kc.auth({
					username: KEYCLOAK_ADMIN_USERNAME,
					password: KEYCLOAK_ADMIN_PASSWORD,
					grantType: "password",
					clientId: "admin-cli",
				});

				console.log("Keycloak is online!");
				resolve(true);
			}
			catch (err) {
				console.error(err);
				setTimeout(() => resolve(false), 2000);
			}
		});

		if (isKeycloakOnline) {
			break;
		}
	}

	return kc;
};

export default getClient;
