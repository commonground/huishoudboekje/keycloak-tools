import yargs from "yargs";
import {hideBin} from "yargs/helpers";
import fullImport from "./src/commands/full-import";
import redirectUris from "./src/commands/redirect-uri";

console.log("Welcome to keycloak-tools!");
console.log();

yargs(hideBin(process.argv))
	.command(redirectUris)
	.command(fullImport)
	.demandCommand()
	.help()
	.argv;
